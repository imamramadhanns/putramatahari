	<!DOCTYPE HTML>
	<html>
	
	<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	
	<link href="fonts/ostrich_sans/stylesheet.css" rel="stylesheet" type="text/css" />
	<link href="fonts/pt_sans/stylesheet.css" rel="stylesheet" type="text/css" />
	<link href="style.css" rel="stylesheet" type="text/css" />
	<link href="css/header_style.css" rel="stylesheet" type="text/css" />
	<link href="css/widgets.css" rel="stylesheet" type="text/css" />
	<link href="css/columns.css" rel="stylesheet" type="text/css" />
	<link href="css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="css/prettyPhoto.css" rel="stylesheet" type="text/css"/>
	<link href="css/usquare_style.css" rel="stylesheet" type="text/css" />
	<link href="css/usquare_logos_style.css" rel="stylesheet" type="text/css" />
	<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
	<link href="css/jcarousel.css" rel="stylesheet" type="text/css" /> 
	
	<!--[if IE 9]>
		<link rel="stylesheet" type="text/css" href="style_IE9.css" />
	<![endif]-->
	
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="style_IE.css" />
	<![endif]-->
	
	<title>Our Team</title>
	
	
	<script type="text/javascript" src="js/jquery-1.8.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.color.js"></script>
	<script type="text/javascript" src="js/ie.html5.js"></script>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/sidebar.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
	
	
	
	<script src="../../ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
	<script src="js/jquery.mousewheel.min.js"></script>
	<script src="js/jquery.mCustomScrollbar.min.js"></script>
	<script src="js/jcarousel.min.js"></script>
	
	<script src="js/jquery.usquare.js" type="text/javascript"></script>
	</head>
	
	<style>
		.usquare_block {height: 300px}
		.usquare_square {width: 320px;height: 300px}
		.usquare_square_text_wrapper {position:inherit;width: 285px;padding: 15px}
		.usquare_module_wrapper h2 {border-bottom: 1px solid #fff;margin-bottom: 10px;font-size:20px}
		.usquare_module_wrapper li {padding:0; margin:0px 15px;color:#fff;font-size:13px;list-style-type:unset}
	</style>
	<body>
	<div id="content">
		<h2 class="separator_h2">Price list Putra matahari event organizer</h2>
		<div class="separator margin_bottom"></div>
		<div class="usquare_module_wrapper usquare_logos_style">		
			<div class="usquare_block">
				<div class="usquare_square usquare_square_bg4">
					<div class="usquare_square_text_wrapper">
						<div class="clear"></div>
						<h2><strong>Paket Team building I Rp 175.000,-/orang</strong></h2>
						<span>Fasilitas :</span>
						<ul>
							<li>Free tiket masuk </li>
							<li>Meals; 1 kali makan</li>
							<li>Design program</li>
							<li>Low rope game</li>
							<li>High rope games</li>
							<li>Arung jeram 600 m</li>
							<li>Penggunaan Aula/saung/lapangan</li>
						</ul>
						<span>Minimum 50 orng</span>
						<br>
						<div class="clear"></div>
					</div>
				</div>
			</div>	          		
			<div class="usquare_block">
				<div class="usquare_square usquare_square_bg5">
					<div class="usquare_square_text_wrapper">
						<div class="clear"></div>
						<h2><strong>Paket Team building II Rp 250.000,-/orang</strong></h2>
						<span>Fasilitas :</span>
						<ul>
							<li>Free tiket masuk </li>
							<li>Meals; 1 kali makan</li>
							<li>Design program</li>
							<li>Low rope game</li>
							<li>High rope games</li>
							<li>Arung jeram 600 m</li>
							<li>Paint ball</li>
							<li>Penggunaan Aula/saung/lapangan</li>
						</ul>
						<span>Minimum 50 orng</span>
						<br>
						<div class="clear"></div>
					</div>
				</div>
			</div>	             		
			<div class="usquare_block">
				<div class="usquare_square usquare_square_bg6">
					<div class="usquare_square_text_wrapper">
						<div class="clear"></div>
						<h2 style="font-size: 18px;"><strong>Paket Family gathering Rp 150.000,-/orang</strong></h2>
						<span>Fasilitas :</span>
						<ul>
							<li>Free tiket masuk </li>
							<li>Free 4 wahana permainan</li>
							<li>Meals; 1 kali makan 1 kali snack</li>
							<li>Tenda + kursi dan panggung</li>
							<li>Organ tunggal</li>
							<li>Fun games </li>
							<li>Fasilitator/pemandu</li>
							<li>Welcome banner</li>
							<li>Penggunaan Aula/saung/lapangan</li>
							
						</ul>
						<span>Minimum 50 orng</span>
						<br>
						<div class="clear"></div>
					</div>
				</div>
			</div>	              		
			<div class="usquare_block">
				<div class="usquare_square usquare_square_bg7">
					<div class="usquare_square_text_wrapper">
						<div class="clear"></div>
						<h2><strong>Paket Fun game Rp 120.000,-/orang</strong></h2>
						<span>Fasilitas :</span>
						<ul>
							<li>Free tiket masuk </li>
							<li>Free 4 wahana permainan</li>
							<li>Meals; 1 kali makan</li>
							<li>Fun games session</li>
							<li>Fasilitator</li>
							<li>Sound system</li>
							<li>Penggunaan Aula/saung/lapangan</li>
						</ul>
						<span>Minimum 50 orng</span>
						<br>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			
			<div class="usquare_block">
				<div class="usquare_square usquare_square_bg8">
					<div class="usquare_square_text_wrapper">
						<div class="clear"></div>
						<h2><strong>Paket paint ball Rp 125.000,-/orang</strong></h2>
						<span>Fasilitas :</span>
						<ul>
							<li>Free tiket masuk </li>
							<li>Area tempur</li>
							<li>Senjata semi otomatis</li>
							<li>Body protector</li>
							<li>Instruktur</li>
							<li>Peluru 30 butir</li>
							<li>Air mineral</li>
							<li>Air mineral</li>
						</ul>
						<span>Minimum 50 orng</span>
						<br>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			
			<div class="usquare_block">
				<div class="usquare_square usquare_square_bg9">
					<div class="usquare_square_text_wrapper">
						<div class="clear"></div>
						<h2><strong>Paket outbound kids Rp 120.000,-/orang</strong></h2>
						<span>Fasilitas :</span>
						<ul>
							<li>Free tiket masuk</li>
							<li>Meals; 1 kali makan</li>
							<li>Design program</li>
							<li>Simulation for kids</li>
							<li>High ropes session</li>
							<li>fasilitator</li>
							<li>Penggunaan Aula/saung/lapangan</li>

						</ul>
						<span>Minimum 50 orng</span>
						<br>
						<div class="clear"></div>
					</div>
				</div>
			</div>	 
			<div class="clear"></div>
			<div class="usquare_block">
				<div class="usquare_square usquare_square_bg4">
					<div class="usquare_square_text_wrapper">
						<div class="clear"></div>
						<h2><strong>Paket perpisahaan  sekolah Rp 120.000,-/orang</strong></h2>
						<span>Fasilitas :</span>
						<ul>
							<li>Free tiket masuk </li>
							<li>Free 4 wahana permainan </li>
							<li>Meals; 1 kali makan 1 kali snack</li>
							<li>Dekorasi</li>
							<li>Welcome banner</li>
							<li>Sound system</li>
							<li>Fasilitator/pemandu</li>
							<li>Penggunaan Aula/saung/lapangan</li>
						</ul>
						<span>Minimum 50 orng</span>
						<br>
						<div class="clear"></div>
					</div>
				</div>
			</div>

			<div class="usquare_block">
				<div class="usquare_square usquare_square_bg5">
					<div class="usquare_square_text_wrapper">
						<div class="clear"></div>
						<h2><strong>Paket one day tour ( ODT ) Rp 90.000,-/orang</strong></h2>
						<span>Fasilitas :</span>
						<ul>
							<li>Free tiket masuk </li>
							<li>Free 4 wahana permainan</li>
							<li>Meals; 1 kali makan 1 kali snack</li>
							<li>Fasilitator/pemandu</li>
							<li>Penggunaan Aula/saung/lapangan</li>
						</ul>
						<span>Minimum 30 orng</span>
						<br>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			<div class="usquare_block">
				<div class="usquare_square usquare_square_bg6">
					<div class="usquare_square_text_wrapper">
						<div class="clear"></div>
						<h2 ><strong>Paket Full day tour (FDT) Rp 150.000 ,-/orang</strong></h2>
						<span>Fasilitas :</span>
						<ul>
							<li>Free tiket masuk </li>
							<li>Free 3 wahana permainan</li>
							<li>Meals; 3 kali makan</li>
							<li>Room penginapan barak</li>
							<li>Penggunaan Aula/saung</li>
							
							
						</ul>
						<span>Minimum 30 orng</span>
						<br>
						<div class="clear"></div>
					</div>
				</div>
			</div>	 
			<div class="clear"></div>
			<div class="usquare_block">
				<div class="usquare_square usquare_square_bg1">
					<div class="usquare_square_text_wrapper">
						<div class="clear"></div>
						<h2><strong>Paket Agro sawah Rp 80.000,-/orang</strong></h2>
						<span>Fasilitas :</span>
						<ul>
							<li>Free tiket masuk </li>
							<li>Penggunaan Aula/saung </li>
							<li>Meals; 1 kali makan + souvenir</li>
							<li>Bajak sawah,tanam padi,tumbuk padi,tangkap ikan</li>
						</ul>
						<span>Minimum 30 orng</span>
						<br>
						<div class="clear"></div>
					</div>
				</div>
			</div>


			<div class="usquare_block">
				<div class="usquare_square usquare_square_bg2">
					<div class="usquare_square_text_wrapper">
						<div class="clear"></div>
						<h2><strong>Paket Agro sayur Rp 45.000,-/orang</strong></h2>
						<span>Fasilitas :</span>
						<ul>
							<li>Meals; 1 kali makan  </li>
							<li>Cara membuat kompos dan menanam sayur</li>
							<li>Pembelajaran tentang jamur,sayur</li>
							<li>Praktek menanam dan memanen sayur</li>
							<li>Memberi makan kelinci dan kambing</li>
							<li>Penggunaan gazebo/saung</li>

						</ul>
						<span>Minimum 30 orng</span>
						<br>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			<div class="usquare_block">
				<div class="usquare_square usquare_square_bg3">
					<div class="usquare_square_text_wrapper">
						<div class="clear"></div>
						<h2 ><strong>Rafting</strong></h2>

						<ul>
							<li>Family trip jarak 4 km /1 jam   Rp 150.000,-/orang</li>
							<li>Fun trip jarak 5 km / 1,5 jam   Rp 175.000,-/orang</li>
							<li>Adventure trip jarak 8 km/2 jam Rp  250.000,-orang</li>
							
						</ul>

						<br>
						<div class="clear"></div>
					</div>
				</div>
			</div>	 
						<div class="clear"></div>
		</div>
	</div>