﻿<?php
@session_start();
include "include/application_top.php";
if($_SESSION['usernameputramatahari']==""){
		echo "<script>;document.location.href='login.php'</script>";
		
		exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="../images/pavicon.png" />

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Form Controls | Putra Matahari Admin</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
    <link href="css/fancy-button/fancy-button.css" rel="stylesheet" type="text/css" />
    <!--Jquery UI CSS-->
    <link href="css/themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <!--jQuery Date Picker-->
    <script src="js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.datepicker.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.progressbar.min.js" type="text/javascript"></script>
    <!-- jQuery dialog related-->
    <script src="js/jquery-ui/external/jquery.bgiframe-2.1.2.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.draggable.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.position.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.resizable.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.ui.dialog.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.blind.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui/jquery.effects.explode.min.js" type="text/javascript"></script>
    <!-- jQuery dialog end here-->
    <script src="js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <!--Fancy Button-->
    <script src="js/fancy-button/fancy-button.js" type="text/javascript"></script>
    <script src="js/setup.js" type="text/javascript"></script>
    <!-- Load TinyMCE -->
    <script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>

</head>
<body>
    <div class="container_12">
        <div class="grid_12 header-repeat">
            <div id="branding">
                <div class="floatleft">
                  <h2 style="color:#FFF"> <strong>Putra Matahari Administrator</strong></h2></div>
                <div class="floatright">
                    <div class="floatleft">
                        <img src="img/img-profile.jpg" alt="Profile Pic" /></div>
                    <div class="floatleft marginleft10">
                        <ul class="inline-ul floatleft">
                            <li>Hello <?php echo  $_SESSION['usernameputramatahari'];?></li>
                            <li><a href="modules/prosess_logout.php">Logout</a></li>
                        </ul>
                        <br />
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="grid_12">
            <ul class="nav main">
                <li class="ic-dashboard"><a href="#"><span>Admin</span></a> 
                    <ul>
                        <li><a href="image-gallery.html">Cek testimonial</a> </li>
                        <li><a href="home.php?Page=Admin">Input Admin</a> </li>
                    </ul>

                </li>
                
                <li class="ic-form-style"><a href="home.php?Page=Produk"><span>Input Produk</span></a> </li>
				<li class="ic-typography"><a href="typography.html"><span>Input Paket</span></a></li>
                <li class="ic-charts"><a href="#"><span>Input Kontent</span></a>
                    <ul>
                        <li><a href="home.php?Page=Logo">Input Logo</a> </li>
                        <li><a href="home.php?Page=Slider">Input Foto Slider</a> </li>
                        <li><a href="home.php?Page=Profile">Input Profile</a> </li>
                    </ul>

                </li>
                
                <li class="ic-grid-tables"><a href="#"><span>Input Kontak</span></a>
                     <ul>
                        <li><a href="home.php?Page=Email">Input Email</a> </li>
                        <li><a href="home.php?Page=HP">Input No. Hp</a> </li>
                        <li><a href="home.php?Page=Telphone">Input No. Telphone</a> </li>
                    </ul>
</li>
                <li class="ic-gallery dd"><a href="#"><span>Input Galery/foto</span></a>
               		 <ul>
                        <li><a href="home.php?Page=Galery">Input Galery</a> </li>
                        <li><a href="home.php?Page=Foto">Input Foto</a> </li>
                    </ul>
                </li>
                <li class="ic-notifications"><a href="home.php?Page=Berita"><span>Input Berita</span></a></li>

            </ul>
        </div>
        <div class="clear">
        </div>
        <div class="grid_12">
            <div class="box round first fullpage">
                <h2><?php echo $_GET['Page']?></h2>
                <div class="block ">
                  <?php
                  
				  if($_GET['Page']=="Admin"){
					include "admin.php";
				  }elseif($_GET['Page']=="Produk"){
					include "upload_produk.php";
				  }elseif($_GET['Page']=="Berita"){
					include "Berita.php";
				  }elseif($_GET['Page']=="Email"){
					include "Email.php";
				  }elseif($_GET['Page']=="Telphone"){
					include "telphone.php";
				  }elseif($_GET['Page']=="Galery"){
					include "Galery.php";
				  }elseif($_GET['Page']=="HP"){
					include "HP.php";
				  }elseif($_GET['Page']=="Foto"){
					include "Foto.php";
				  }elseif($_GET['Page']=="Slider"){
					include "slider.php";
				  }elseif($_GET['Page']=="Profile"){
					include "Profile.php";
				  }elseif($_GET['Page']=="Logo"){
					include "Logo.php";

				  }
				  ?>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <div id="site_info">
        <p>Copy Right <?PHP echo date('Y')?>
           &nbsp; <a href="http://www.muhamadrusdi.etingi.com">&copy; Syahren</a>
        </p>
    </div>
</body>
</html>
