
(function($){




/* ---------------------------------------------------------- */
/* HEADER */
/* ---------------------------------------------------------- */

// Main menu dropdown animations
$(document).ready(function(){
	var res440menu = ($('body').width() <= 735);
	xMainMenuSetup(res440menu);

	$(window).resize(function () {
		xResizeDelay(function(){
			if (res440menu != ($('body').width() <= 735)) {
				if (res440menu) {
					var liText = '.main_menu li li';
					while ($(liText + ' a span').length > 0) {
						$(liText + ' a span').each(function(){
							$(this).html($(this).html().substr(3))
						});
						liText += ' li';
					}
				}
				res440menu = ($('body').width() <= 735);
				$('#menu_wrapper').unbind();
				$('.menu_arrow').removeClass('active');
				$('.main_menu').attr('style', '').unbind().removeData().find('li').attr('style', '').unbind().removeData();
				$('.main_menu').find('span').stop(true).attr('style', '');
				xMainMenuSetup(res440menu);
			}
		}, 200, "mainMenu");
	});
	
});

function xMainMenuSetup (res440) {
	if (res440) {
		var liText = '.main_menu li li';
		var menuOn = false;
		while ($(liText + ' a span').length > 0) {
			$(liText + ' a span').prepend(' - ');
			liText += ' li';
		}
		
		$('#menu_wrapper').click(function(){ 
			if(!menuOn) {
				$(this).find('.main_menu').show();
				$(this).find('.menu_arrow').addClass('active');
				menuOn = true;
			}
			else {
				menuOn = false;
				$(this).find('.main_menu').hide();
				$(this).find('.menu_arrow').removeClass('active');
			}
			
		});
		
	}
	else {
		$('.main_menu').children('li').hover(function(){
			$(this).data('xHovered', 1);
			$(this).children('a').find('span:first').stop(true).animate({color : '#ffffff'}, 400);
			$(this).children('a').find('span.menu_background').stop(true).animate({left : 0}, 400);
			xSquareBusyTimeout($(this));
		}, function(){
			$(this).data('xHovered', 0);
			$(this).children('ul').xSquareMenuList(true);
			xSquareLinkTimeout($(this));
		});
		$('.main_menu').children('li').find('li').hover(function(){
			$(this).children('ul').xSquareMenuList(false);
		},function(){
			$(this).children('ul').xSquareMenuList(true);
		});
	}

}


function xSquareBusyTimeout($this) {
	if ($this.children('ul').length > 0 && $this.parent('.main_menu').data('xBusy') == 1 && $this.data('xBusy') != 1) {
		$this.data('xBusyTime', setTimeout(function(){xSquareBusyTimeout($this)},100));
	}
	else if ($this.children('ul').length > 0 && $this.data('xHovered') == 1) {
		$this.children('ul').xSquareMenuList(false);
		$this.data('xBusy', 1);
		$this.parent('.main_menu').data('xBusy', 1);
	}
}

// Scroll in submenu UL
$.fn.xSquareMenuList = function(isOpen) {
	var $this = $(this),
		$items = $this.children('li'),
		itemCount = $items.length,
		count = $this.data('xCount');
	if (typeof count == 'undefined' ) $this.data('xCount', 0);
	if (count < 0) $this.data('xCount', 0);
	if (count > itemCount-1) $this.data('xCount', itemCount-1);
		xSquareMenuTimeout($items, itemCount, isOpen);
		$this.show();
};

function xSquareLinkTimeout($this) {
	if(typeof $this.children('ul:first').data('xCount') == 'undefined' || $this.children('ul:first').data('xCount') < 0) {
		$this.children('a').find('span:first').stop(true).animate({color : '#5a5a5a'}, 400);
		$this.children('a').find('span.menu_background').stop(true).animate({left : '-100%'}, 400);
	}
	else {
		$this.data('xTime', setTimeout(function(){xSquareLinkTimeout($this)},100));
	}

}


// Submenu LI element recursion
function xSquareMenuTimeout($items, itemCount, isOpen) {
	var $parent = $items.eq(0).parent('ul');
	var count = $parent.data('xCount');
	var $this = $items.eq(count);
	clearTimeout($parent.data('xTime'));

	if ((count < itemCount && !isOpen) || (count > -1 && isOpen)) {
		if(!isOpen) {
			$this.show().children('a').find('span').stop(true).animate({left : 0}, 200);
			$parent.data('xCount', count+1);
			$parent.data('xTime', setTimeout(function(){xSquareMenuTimeout($items, itemCount, isOpen)},100));
		}
		else {
			if(typeof $this.children('ul:first').data('xCount') == 'undefined' || $this.children('ul:first').data('xCount') < 0) {
				$this.show().children('a').find('span').stop(true).animate({left : '-100%'}, 200, function(){ $this.hide();});
				$parent.data('xCount', count-1);
				$parent.data('xTime', setTimeout(function(){xSquareMenuTimeout($items, itemCount, isOpen)},100));
			}
			else {
				$parent.data('xTime', setTimeout(function(){xSquareMenuTimeout($items, itemCount, isOpen)},100));
			}
		}
	}
	if(count == -1 && isOpen && $parent.parent('li').data('xBusy') == 1) {
		$parent.parent('li').parent('.main_menu').data('xBusy', 0);
		$parent.parent('li').data('xBusy', 0);
	}
}


/* ---------------------------------------------------------- */
/* PRODUCTS */
/* ---------------------------------------------------------- */

$(document).ready(function(){
	$('.slide_in_block').each(function(){
		$(this).css({bottom : '-'+($(this).height())+'px', height : $(this).height()-10, width: '100%'});
	});
	
	$('.image_module li').css('border-bottom', '0px');
	$('.fade_in_block').hide();
	$('.image_module li .date').hide();
	
	$('.product_image').each(function(){
		var $img = $(this).find('img');
		$img = $img[0];
		      
		if(!$.browser.msie){
			$img.onload = function() {
				$(this).parent().append('<img class="grayscale_image" src="' + grayscaleImage($img) + '" />');
			}
        }
		
	});
	$('.image_module li').hover(function(){
		
		$(this).find('.grayscale_image').hide();
		$(this).find('.date').stop(true, true).fadeIn(300);
	
		$(this).data('fadingIn', true);
		var $block = $(this).find('.slide_in_block');
		$block.stop(true,true).animate({bottom : 0}, 300, function(){$block.find('.fade_in_block').stop(true, true).fadeIn(200)});
	}, function(){
		$(this).find('.grayscale_image').show();
		$(this).find('.date').stop(true, true).fadeOut(300);
		var $this = $(this);
		$this.data('fadingIn', false);
		var $block = $this.find('.slide_in_block');
		$block.find('.fade_in_block').stop(true, true).fadeOut(200, function(){
			if(!$this.data('fadingIn')) {
				$block.stop(true).animate({bottom : '-'+($block.height()+10)+'px'}, 300);
			}
		});
	})
	
});


/* ---------------------------------------------------------- */
/* Grayscale */
/* ---------------------------------------------------------- */

    function grayscaleImageIE(imgObj)
    {
        imgObj.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(grayScale=1)';
    }

    function grayscaleRemoveIE(imgObj)
    {
        imgObj.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(grayScale=0)';
    }

    function grayscaleImage(imgObj)
    {
        var canvas = document.createElement('canvas');
        var canvasContext = canvas.getContext('2d');
        
        var imgW = imgObj.width;
        var imgH = imgObj.height;
        canvas.width = imgW;
        canvas.height = imgH;
        
        canvasContext.drawImage(imgObj, 0, 0);
        var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
        
        for(var y = 0; y < imgPixels.height; y++){
            for(var x = 0; x < imgPixels.width; x++){
                var i = (y * 4) * imgPixels.width + x * 4;
                var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
                imgPixels.data[i] = avg; 
                imgPixels.data[i + 1] = avg; 
                imgPixels.data[i + 2] = avg;
            }
        }
        
        canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
        return canvas.toDataURL();
    }









/* ---------------------------------------------------------- */
/* SOCIAL ICONS */
/* ---------------------------------------------------------- */
$(document).ready(function(){
	$('.social_holder a').each(function(){
		var $this = $(this);
		$this.append($this.html());
		$this.find('img').eq(0).addClass('mainSoc');
		$this.find('img').eq(1).addClass('hoverSoc');
	});
	
	$('.social_holder a').hover(function() {
		$(this).find('.mainSoc').stop(true).animate({top : '-39px'}, 300);
		$(this).find('.hoverSoc').stop(true).animate({top : '0px'}, 300);
	}, function() {
		$(this).find('.mainSoc').stop(true).animate({top : '0px'}, 300);
		$(this).find('.hoverSoc').stop(true).animate({top : '39px'}, 300);
	});
});



/* ---------------------------------------------------------- */
/* BLOG NAVIGATION */
/* ---------------------------------------------------------- */
$(document).ready(function(){
	$('.blog_navigation a').each(function(){
		var h = $(this).height();
		var w = ($(this).width()+16);
		$(this).css({width: w + 'px', height : h + 'px', paddingRight:0, paddingLeft:0});
		var text = $(this).html();
		$(this).wrapInner('<span class="nav_inner" style="height: '+h+'px;" />');
		$(this).append('<span class="nav_inner_hover" style="height: '+h+'px;">'+text+'</span>');
	});
	
	
	$('.blog_navigation a').hover(function() {
		$(this).find('.nav_inner').stop(true).animate({top : '-100%'}, 300);
		$(this).find('.nav_inner_hover').stop(true).animate({top : '0px'}, 300);
	}, function() {
		$(this).find('.nav_inner').stop(true).animate({top : '0px'}, 300);
		$(this).find('.nav_inner_hover').stop(true).animate({top : '100%'}, 300);
	});
});



/* ---------------------------------------------------------- */
/* IMAGE HOVER */
/* ---------------------------------------------------------- */
$(document).ready(function(){
	
	$('.image_holder').hover(function(){
		$(this).find('.image_hover_links .image_left_arrow').stop(true).animate({marginLeft : 0}, 200);
		$(this).find('.image_hover_links .image_right_arrow').stop(true).animate({marginRight : 0}, 200);
		$(this).find('.image_hover_links a').each(function(index){
			var del = index*100;
			$(this).find('img').delay(del).animate({top : 0}, 200);
		
		});
	}, function(){
		$(this).find('.image_hover_links .image_left_arrow').stop(true).animate({marginLeft : '-40px'}, 200);
		$(this).find('.image_hover_links .image_right_arrow').stop(true).animate({marginRight : '-40px'}, 200);
		$(this).find('.image_hover_links a img').stop(true).animate({top : '40px'}, 200);
	});
});



/* ---------------------------------------------------------- */
/* PRODUCTS PAGE */
/* ---------------------------------------------------------- */

$.fn.xSquareProducts = function(options) {
	var settings = $.extend({
		pagesArray : [],
		startPageIndex : 0,
		priceTitle : 'PRICE & INFO',
		orderTitle : 'ORDER NOW',
		quantityLabel : 'Quantity:',
		orderButtonText : 'ADD TO CART'
	}, options)
	
	var $this = this,
		pIndex = settings.startPageIndex,
		page = settings.pagesArray[pIndex];
	
		
	$this.data('xProducts', {
		settings : settings,
		currentPage : pIndex,
		currentImage : 0
	});
	
	var data = $this.data('xProducts');
	var currentPage = data.currentPage;
	var frameHtml = '\n'+


'		<h2 class="product_page_title separator_h2 margin_top">'+settings.pagesArray[currentPage].main_title+'</h2> \n'+
'		<div class="separator margin_bottom"></div> \n'+
	
'		<div class="product_page_slider_controles"> \n'+
'			<a href="#" class="products_page_right'+((pIndex == settings.pagesArray.length-1) ? ' desabled' : '')+'"></a> \n'+
'			<a href="#" class="products_page_left'+(pIndex == 0 ? ' desabled' : '')+'"></a> \n'+
'			<div class="clear"></div> \n'+
'		</div> \n'+
	
'		<div class="product_page_slider_image_wrapper"> \n'+
'			<div class="product_pate_slider_image_holder"></div> \n'+
'			<img class="product_page_slider_loader" src="images/ajax-loader.gif" />'+
'		</div><!-- product_page_slider_image_wrapper --> \n'+

'		<div class="product_page_slider_text_wrapper"> \n'+
'			<div class="products_page_slider_text_holder">'+
'			</div>'+
'		</div><!-- product_page_slider_text_wrapper --> \n'+

'		<div class="clear"></div> \n'+
'		<div class="separator2 margin_top margin_bottom"></div> \n'+

'		<div class="product_small_product_blocks_wrapper"> \n'+
'			<div class="product_small_product_blocks_holder"> \n'+

'			</div><!-- product_small_product_blocks_holder --> \n'+
'		</div><!-- product_small_product_blocks_wrapper --> \n'+

'		<div class="clear"></div> \n'+
'		<div class="separator2 margin_top margin_bottom"></div> \n'+
		
'		<div class="offer_block_wrapper"> \n'+
'			<div class="price_info"> \n'+
'				<div class="product_movable_text_wrapper"></div> \n'+
'			</div><!-- price_info --> \n'+
'			<div class="order_now"> \n'+
'				<div class="product_movable_text_wrapper"></div> \n'+
'			</div><!-- order_now --> \n'+
'			<div class="clear"></div> \n'+
'		</div><!-- offer_block_wrapper -->';

	$this.html(frameHtml);
	
	
	// SLIDER IMAGE
	xLoadImage(page.images[0], $this.find('.product_pate_slider_image_holder'), $this.find('.product_page_slider_loader'));
	
	// SLIDER TEXT
	xSwapProductText(0, $this);
	
	
	// SMALL BLOCK
	xSwapProductThumbnails($this);
	
	
	// PRICE INFO
	xSwapPriceInfo ($this);
	
	// ORDER INFO
	xSwapOrderInfo($this);
	
	

	
	$this.find('.small_product_block').live('click', function(){
		if (!$(this).hasClass('active')) {
			var $select = $this.find('.small_product_block'),
				blockIndex = $select.index($(this)),
				data = $this.data('xProducts'),
				page = data.settings.pagesArray[data.currentPage];
				
			$select.removeClass('active');
			$(this).addClass('active');
			data.currentImage = blockIndex;
			xLoadImage(page.images[blockIndex], $this.find('.product_pate_slider_image_holder'), $this.find('.product_page_slider_loader'), true);
			xSwapProductText(blockIndex, $this, true);
			
		}

	});
	
	$this.find('.products_page_right').click(function(e) {
		e.preventDefault();
		var data = $this.data('xProducts');
		if(!$(this).hasClass('desabled')) {
			$this.find('.products_page_left').removeClass('desabled');
			if(data.currentPage == settings.pagesArray.length-2) $(this).addClass('desabled');
			xSwapProductPage(data.currentPage+1, $this);
		}
	});
	
	
	$this.find('.products_page_left').click(function(e) {
		e.preventDefault();
		var data = $this.data('xProducts');
		if(!$(this).hasClass('desabled')) {
			$this.find('.products_page_right').removeClass('desabled');
			if(data.currentPage == 1) $(this).addClass('desabled');
			xSwapProductPage(data.currentPage-1, $this);
		}
	});
}

function xSwapProductPage (newIndex, $this) {
	var data = $this.data('xProducts'),
		page = data.settings.pagesArray[newIndex];
	
	data.currentPage = newIndex;
	
	$this.find('.product_page_title').html(page.main_title); 
	
	// IMAGE SWAP
	xLoadImage(page.images[0], $this.find('.product_pate_slider_image_holder'), $this.find('.product_page_slider_loader'), true);
	
	// SLIDER TEXT SWAP
	xSwapProductText(0, $this, true);
	
	// SMALL BLOCK SWAP
	xSwapProductThumbnails($this, true);
	
	// PRICE INFO
	//xSwapPriceInfo ($this, true);
	
	// ORDER INFO
	//xSwapOrderInfo($this, true);
}


function xLoadImage(src, $path, $loader, transition) {
	$loader.show();
	var img = $("<img />").attr('src', src)
	    .load(function() {
	        if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
	            alert('Image ' + src + ' could not load!');
	        } else {
	            $path.html(img);
				if(transition) {
					$path.find('img').hide().fadeIn(300);
				}
	        }
			if(transition) {
				$loader.fadeOut(300);
			}
			else {
				$loader.hide();
			}
	    });
}

function xSwapProductText(newIndex, $this, transition) {
	var data = $this.data('xProducts'),
		page = data.settings.pagesArray[data.currentPage],
		$wrapper = $this.find('.product_page_slider_text_wrapper');
	
	var html = '<h3 class="margin_bottom">'+page.titles[newIndex]+'</h3>' + '<span>' + page.descriptions[newIndex] + '</span>';
		html += '<div class="blog_post_social_links"><ul>';
	
	$.each(page.socialLinks[newIndex],function(i, val){
		if(typeof val != 'undefined') {
			html += '<li><a href="'+val +'" class="social_' + i + '"></a></li>';
			
		}
	});
	html += '</ul></div><div class="clear"></div>';
	
	html = '\n'+
'			<div class="products_page_slider_text_holder'+(transition ? ' absolute' : '')+'">'+ html +'</div> \n';
	if(transition) {
		$wrapper.find('.products_page_slider_text_holder').stop(true).animate({opacity : 0}, 300, function(){$(this).remove()});
		$wrapper.append(html);
		var $newElem = $wrapper.find('.products_page_slider_text_holder:last');
		$newElem.animate({opacity : 1.0}, 300, function(){$(this).removeClass('absolute')});
		$wrapper.animate({'height' : $newElem.height() +'px'}, 300);
	}
	else {
		$wrapper.html(html);
	}

}

function xSwapProductThumbnails($this, transition) {
	var data = $this.data('xProducts'),
		page = data.settings.pagesArray[data.currentPage];
	
	if(transition) {
		$this.find('.product_small_product_blocks_wrapper').stop(true).animate({'height' : 0 }, 300
			,function(){
				// PRICE INFO
				xSwapPriceInfo ($this, true);
				
				// ORDER INFO
				xSwapOrderInfo($this, true);
		});
	}
	
	var html = '';
	var imageCount = 0;
	for (i=0; i<page.thumbnails.length; i++) {
		html += '\n'+
'				<div class="small_product_block '+((i%4 == 3) ? 'last' : '')+((i == 0) ? ' active' : '')+'"> \n'+
'					<img src="'+page.thumbnails[i]+'" alt="image" /> \n'+
'					<span class="small_product_block_text"><span>'+page.titles[i]+'</span>'+page.descriptions[i].substr(0, 20)+'</span> \n'+
'				</div><!-- small_product_block -->'+
				((i%4 == 3) ? '\n				<div class="clear"></div>\n' : '');
	
	var $dialog = $('<div><img src="'+page.thumbnails[i]+'" /></div>');
	var $imgElement = $dialog.find('img');

	$imgElement.load(function() {
	    imageCount ++;
		if (imageCount == page.thumbnails.length) {
			if(transition) {
				$this.find('.product_small_product_blocks_wrapper').delay(500).animate({'height' : $this.find('.product_small_product_blocks_holder').height() +'px' }, 300);
			}
			else {
				$this.find('.product_small_product_blocks_wrapper').css({'height' : $this.find('.product_small_product_blocks_holder').height() +'px' });
			}
		}
	});
		
		
	}
	html += '\n				<div class="clear"></div>\n';
		
	$this.find('.product_small_product_blocks_holder').html(html);
}

function xSwapPriceInfo ($this, transition) {
	var data = $this.data('xProducts'),
		page = data.settings.pagesArray[data.currentPage],
		$wrapper = $this.find('.price_info .product_movable_text_wrapper');

	var html = '\n'+	
'				<span><span>'+data.settings.priceTitle+'</span>'+page.priceText+'</span> \n'+
'					<div class="clear"></div> \n'+
((typeof page.oldPrice != 'undefined') ? '				<div class="price"><div></div>350.00</div><!-- price --> \n'+ '					<div class="clear"></div> \n' : '')+
((typeof page.newPrice != 'undefined') ? '				<div class="discount">299.99</div><!-- discount --> \n' : '');

	html = '<div class="product_movable_text_holder'+(transition ? ' absolute' : '')+'">'+html+'<div class="clear"></div></div>';
	if (transition) {
		$wrapper.append(html);
		$wrapper.animate({height : $wrapper.find('.product_movable_text_holder:last').height() + 'px'}, 500);
		$wrapper.find('.product_movable_text_holder:not(:last)').css({'position' : 'absolute'}).animate ({left : '-100%'}, 500, function(){$(this).remove(); });
		$wrapper.find('.product_movable_text_holder:last').css({'position' : 'absolute'}).animate ({left : '0'}, 500, function(){$(this).removeClass('absolute'); });
	}
	else {
		$wrapper.html(html);
	}	
}

function xSwapOrderInfo($this, transition){
	var data = $this.data('xProducts'),
		page = data.settings.pagesArray[data.currentPage],
		$wrapper = $this.find('.order_now .product_movable_text_wrapper');


	var html = ''+
'				<span><span>'+data.settings.orderTitle+'</span>'+page.orderText+'</span> \n'+
'				<span class="quantity">'+data.settings.quantityLabel+'</span> \n'+
'				<input type="text" value="" /> \n'+
'				<a href="'+page.orderLink+'" class="read_more">'+data.settings.orderButtonText+'</a>';

	html = '<div class="product_movable_text_holder'+(transition ? ' absolute' : '')+'">'+html+'<div class="clear"></div></div>';
	if (transition) {
		$wrapper.append(html);
		$wrapper.animate({height : $wrapper.find('.product_movable_text_holder:last').height() + 'px'}, 500);
		$wrapper.find('.product_movable_text_holder:not(:last)').css({'position' : 'absolute'}).animate ({left : '100%'}, 500, function(){$(this).remove(); });
		$wrapper.find('.product_movable_text_holder:last').css({'position' : 'absolute'}).animate ({left : '0'}, 500, function(){$(this).removeClass('absolute'); });
	}
	else {
		$wrapper.html(html);
	}	
	
	
}


/* ---------------------------------------------------------- */
/* RESIZE DELAY */
/* ---------------------------------------------------------- */
var xResizeDelay = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) {
			uniqueId = "noId";
		}
		if (timers[uniqueId]) {
			clearTimeout (timers[uniqueId]);
		}
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();


/* ---------------------------------------------------------- */
/* CONTACT FORM */
/* ---------------------------------------------------------- */
$(document).ready(function(){
	$('.clearTextarea').click(function(e){
		e.preventDefault();
		var $form = $(this).parent();
		while (!$form.is("form")) {
			$form = $form.parent();
		}
		$form.find('textarea').val('').removeClass('empty');
		$form.find('input').val('').removeClass('empty');
	});
	
	$('form .send').click(function(e){
		var $form = $(this).parent();
		while (!$form.is("form")) {
			$form = $form.parent();
		}
		console.log($form);
		var full = true;
		$form.find('.requiered').each(function(){
			if($(this).val() == '') {
				full = false;
				$(this).addClass('empty');
			}
			else {
				$(this).removeClass('empty');
			}
		});
		if (!full) {
			e.preventDefault();
		}
	
	});
	
});


})(jQuery);