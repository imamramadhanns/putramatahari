	
(function($){ 
$(document).ready(function(){
	

	// animation for menu type widgets
	$(".page_item a, .cat_item a").hover(function(){
		$(this).stop(true, true).animate({borderLeftWidth: "15px"},"fast");
	}, function(){
		$(this).stop(true, true).animate({borderLeftWidth: "5px"},"medium");
	})
	
	
	
	// tabs
	$(".tabs .tabs-nav a").click(function(e){
		e.preventDefault();
		if(!$(this).hasClass('active')) {
			$(this).parent().parent().find('a').removeClass("active");
			$(this).addClass('active');
			
			var $containter = $(this).parent().parent().parent().find('.tabs-container'),
				tabId = $(this).attr('href');
				
			$containter.children('.tab-content').stop(true, true).hide();
			$containter.find(tabId).fadeIn();
		}
  });
	$(".tabs a:first").trigger("click");
	
	
	
	 // accordion
 
	 var $container = $('.acc-content'),
	  $trigger   = $('.acc-trigger');

	 $container.hide();
	 $trigger.prepend('<span class="acc-arrow"></span>').first().addClass('active').next().show();

	 var fullWidth = $container.outerWidth(true);
	 $trigger.css('width', fullWidth-75);
	 $container.css('width', fullWidth-75);
	 
	 $trigger.click(function(e) {
	  if( $(this).next().is(':hidden') ) {
	   $(this).parent().find('.acc-trigger').removeClass('active').next().slideUp(300);
	   $(this).toggleClass('active').next().slideDown(300);
	  }
	  e.preventDefault();
	 });
	
	
	});
	})(jQuery);