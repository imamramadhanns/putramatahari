-- MySQL dump 10.13  Distrib 5.5.37, for Linux (x86_64)
--
-- Host: localhost    Database: putramat_putramatahari
-- ------------------------------------------------------
-- Server version	5.5.37-cll

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_berita`
--

DROP TABLE IF EXISTS `tbl_berita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `judul_berita` text COLLATE latin1_general_ci,
  `isi_berita` text COLLATE latin1_general_ci,
  `foto` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `tgl_posting` date DEFAULT NULL,
  `by` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_berita`
--

LOCK TABLES `tbl_berita` WRITE;
/*!40000 ALTER TABLE `tbl_berita` DISABLE KEYS */;
INSERT INTO `tbl_berita` (`id_berita`, `judul_berita`, `isi_berita`, `foto`, `tgl_posting`, `by`) VALUES (2,'TAMAN WISATA MATAHARI','Awal kegiatan Rafting ditemukan oleh Letnan John Fremont dari Amerika Serikat dan dirancang oleh Horace Hari H. pada Tahun 1842','taman-matahari.jpg','2013-05-02','rusdi'),(3,'TWN AWARD','Seorang karyawan diharapkan mampu meningkatkan etos kerjanya. Hal ini dimungkinkan melalui kegiatan Outbound Training','taman wisata matahari award.jpg','2013-04-30','rusdi'),(7,'KARNAVAL','\r\n	Bogor - Kemeriahan hari jadi Bogor ke 531 dan jelang  libur sekolah di jadikan momen untuk mningkatkan jumlah pengunjung dan memperkenalkan kebudayaan daerah di Indonesia. Humas Taman Wisata Matahari- Azwir mengatakan ada kenaikan pengunjung memasuki masa transisi libur sekolah. \"Per hari minggu , (02/06/2013) pukul 08.00 WIB saja sudah 8000 pengunjung , karena tiket masuknya terjangkau Taman Wisata Matahari menjadi wisata yang paling banyak di kunjungi  di kawasan wisata  puncak \". Ujar Azwir, Minggu (02/06).','Taman wisata mataharicarnaval .jpg','2014-06-23','rusdi');
/*!40000 ALTER TABLE `tbl_berita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_foto`
--

DROP TABLE IF EXISTS `tbl_foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_foto` (
  `id_foto` int(11) NOT NULL AUTO_INCREMENT,
  `id_galery` int(11) DEFAULT NULL,
  `foto` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `ukuran_foto` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `tgl_upload` datetime DEFAULT NULL,
  `by` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_foto`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_foto`
--

LOCK TABLES `tbl_foto` WRITE;
/*!40000 ALTER TABLE `tbl_foto` DISABLE KEYS */;
INSERT INTO `tbl_foto` (`id_foto`, `id_galery`, `foto`, `ukuran_foto`, `tgl_upload`, `by`) VALUES (1,1,'taman wisata matahari rafting1.jpg',NULL,'2013-05-05 11:34:55','syahren'),(2,1,'taman wisata matahari rafting2.jpg',NULL,'2013-05-05 11:34:59','syahren'),(14,2,'taman wisata matahari games7.jpg',NULL,'2013-05-05 11:34:59','syahren'),(4,3,'taman wisata matahari outbound8.jpg',NULL,'2013-05-05 11:34:59','syahren'),(5,3,'taman wisata matahari outbound7.jpg',NULL,'2013-05-05 11:34:59','syahren'),(6,3,'taman wisata matahari outbound6.jpg',NULL,'2013-05-05 11:34:59','syahren'),(7,3,'taman wisata matahari outbound5.jpg',NULL,'2013-05-05 11:34:59','syahren'),(8,3,'taman wisata matahari outbound4.jpg',NULL,'2013-05-05 11:34:59','syahren'),(9,3,'taman wisata matahari outbound3.jpg',NULL,'2013-05-05 11:34:59','syahren'),(10,3,'taman wisata matahari outbound2.jpg',NULL,'2013-05-05 11:34:59','syahren'),(11,3,'taman wisata matahari outbound1.jpg',NULL,'2013-05-05 11:34:59','syahren'),(13,2,'taman wisata matahari games8.jpg',NULL,'2013-05-05 11:34:59','syahren'),(12,2,'taman wisata matahari games1.jpg',NULL,'2013-05-05 11:34:59','syahren'),(15,2,'taman wisata matahari games2.jpg',NULL,'2013-05-05 11:34:59','syahren'),(16,2,'taman wisata matahari games3.jpg',NULL,'2013-05-05 11:34:59','syahren'),(17,2,'taman wisata matahari games4.jpg',NULL,'2013-05-05 11:34:59','syahren'),(18,2,'taman wisata matahari games5.jpg',NULL,'2013-05-05 11:34:59','syahren'),(19,2,'taman wisata matahari games6.jpg',NULL,'2013-05-05 11:34:59','syahren'),(22,8,'Jellyfish.jpg','775702','2013-05-18 15:56:46','rusdi');
/*!40000 ALTER TABLE `tbl_foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_galery`
--

DROP TABLE IF EXISTS `tbl_galery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_galery` (
  `id_galery` int(11) NOT NULL AUTO_INCREMENT,
  `nama_galery` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `foto_cover` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `tgl_post` date DEFAULT NULL,
  `by` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_galery`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_galery`
--

LOCK TABLES `tbl_galery` WRITE;
/*!40000 ALTER TABLE `tbl_galery` DISABLE KEYS */;
INSERT INTO `tbl_galery` (`id_galery`, `nama_galery`, `foto_cover`, `tgl_post`, `by`) VALUES (1,'Fun Gathering','Fun Gathering.jpg','2013-05-02','rusdi'),(2,'Team Building Outbound','taman wisata mahatahari cover 2.jpg','2013-05-01','rusdi'),(3,'Perpisahan','taman wisata matahari 39.jpg','2013-05-02','rusdi'),(4,'Agro Sawah','taman wisata matahari 2.jpg','2013-05-02','rusdi');
/*!40000 ALTER TABLE `tbl_galery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_kontak`
--

DROP TABLE IF EXISTS `tbl_kontak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_kontak` (
  `id_contak` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `hp` text COLLATE latin1_general_ci,
  `telphone` text COLLATE latin1_general_ci,
  `alamat` text COLLATE latin1_general_ci,
  `tgl_post` datetime DEFAULT NULL,
  `by` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_contak`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_kontak`
--

LOCK TABLES `tbl_kontak` WRITE;
/*!40000 ALTER TABLE `tbl_kontak` DISABLE KEYS */;
INSERT INTO `tbl_kontak` (`id_contak`, `email`, `hp`, `telphone`, `alamat`, `tgl_post`, `by`) VALUES (1,'zakaria_dmmarket@yahoo.com','0817 9047 558','-','Jalan Raya Puncak KM. 77 No. 132 Cilember, Cisarua Bogor ','2013-05-08 12:18:52',NULL),(11,NULL,'0813 8892 2449',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_kontak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_logo`
--

DROP TABLE IF EXISTS `tbl_logo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_logo` (
  `id_logo` int(11) NOT NULL AUTO_INCREMENT,
  `nama_foto` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `ukuran_foto` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `tgl_upload` date DEFAULT NULL,
  `by` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_logo`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_logo`
--

LOCK TABLES `tbl_logo` WRITE;
/*!40000 ALTER TABLE `tbl_logo` DISABLE KEYS */;
INSERT INTO `tbl_logo` (`id_logo`, `nama_foto`, `ukuran_foto`, `tgl_upload`, `by`) VALUES (1,'logo putmat.png','100','2013-05-03','rusdi');
/*!40000 ALTER TABLE `tbl_logo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_paket`
--

DROP TABLE IF EXISTS `tbl_paket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_paket` (
  `id_paket` int(11) NOT NULL AUTO_INCREMENT,
  `nama_paket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `harga_paket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `min_paket` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `isi_paket` text COLLATE latin1_general_ci,
  `gambar` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `tgl_proses` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `by` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_paket`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_paket`
--

LOCK TABLES `tbl_paket` WRITE;
/*!40000 ALTER TABLE `tbl_paket` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_paket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_produk`
--

DROP TABLE IF EXISTS `tbl_produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_produk` (
  `id_produk` int(11) NOT NULL AUTO_INCREMENT,
  `nama_produk` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `penjelasan` text COLLATE latin1_general_ci,
  `gambar` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `tgl_upload` date DEFAULT NULL,
  `by` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_produk`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_produk`
--

LOCK TABLES `tbl_produk` WRITE;
/*!40000 ALTER TABLE `tbl_produk` DISABLE KEYS */;
INSERT INTO `tbl_produk` (`id_produk`, `nama_produk`, `penjelasan`, `gambar`, `tgl_upload`, `by`) VALUES (1,'Games Outbound','auh sebelum era industry di Eropa, kegiatan outbound sudah diperkenalkan sebagai bagian untuk pengembangan diri (personal development) dan pengembangan team (team development)','taman wisata mahatahari games.jpg','2013-05-02','rusdi'),(2,'Flying Fox','Flying Fox adalah game tantangan individu yang diadaptasi dari pelatihan militer.Game ini dilakukan dengan cara meluncur dari ketinggian tertentu melalui wire(cabel baja)','taman wisata mahatahari flying fox.jpg','2013-05-02','rusdi'),(3,'Rafting','Arung Jeram merupakan sebuah olah raga menantang yang sangat berbahaya, tapi itu bagi orang yang masih awam. Tapi bagi seorang Rafter Profesional, bahanya malahan perjalanan menuju sungai','taman wisata mahatahari Rafting.jpg','2013-05-02','rusdi'),(4,'Paint Ball','Paintball pertama telah dihasilkan oleh Syarikat Cat Nelson pada tahun 1950 untuk tujuan penandaan pokok dari jarak jauh. Permainan paintball pertama telah dimainkan oleh dua sahabat didalam hutan Henniker','taman wisata mahatahari PaintBall.jpg','2013-05-03','rusdi');
/*!40000 ALTER TABLE `tbl_produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_profile`
--

DROP TABLE IF EXISTS `tbl_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_profile` (
  `id_profile` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `penjelasan` text COLLATE latin1_general_ci,
  `visi` text COLLATE latin1_general_ci,
  `misi` text COLLATE latin1_general_ci,
  `tgl_post` date DEFAULT NULL,
  `by` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_profile`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_profile`
--

LOCK TABLES `tbl_profile` WRITE;
/*!40000 ALTER TABLE `tbl_profile` DISABLE KEYS */;
INSERT INTO `tbl_profile` (`id_profile`, `gambar`, `penjelasan`, `visi`, `misi`, `tgl_post`, `by`) VALUES (1,'logo2.png','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n','2013-05-02','rusdi');
/*!40000 ALTER TABLE `tbl_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_slider`
--

DROP TABLE IF EXISTS `tbl_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_slider` (
  `id_slide` int(11) NOT NULL AUTO_INCREMENT,
  `nama_foto` text COLLATE latin1_general_ci,
  `judul_foto` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `tgl_upload` date DEFAULT NULL,
  `by` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_slide`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_slider`
--

LOCK TABLES `tbl_slider` WRITE;
/*!40000 ALTER TABLE `tbl_slider` DISABLE KEYS */;
INSERT INTO `tbl_slider` (`id_slide`, `nama_foto`, `judul_foto`, `tgl_upload`, `by`) VALUES (1,'slider.jpg','GAMES','2013-05-02','rusdi'),(2,'slider2.jpg','FLYING FOX','2013-05-02','rusdi'),(3,'slider3.jpg','FUN GATHERING','2013-05-02','rusdi');
/*!40000 ALTER TABLE `tbl_slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_testi`
--

DROP TABLE IF EXISTS `tbl_testi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_testi` (
  `id_testi` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `komentar` text COLLATE latin1_general_ci,
  `tgl_post` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_testi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_testi`
--

LOCK TABLES `tbl_testi` WRITE;
/*!40000 ALTER TABLE `tbl_testi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_testi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `password` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `level` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `tgl_proses` datetime DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`id_user`, `username`, `password`, `level`, `tgl_proses`, `tgl_update`) VALUES (1,'rusdi','b39e132e649db9527191cf15a6fe90af','1','2013-05-07 19:28:17','2014-10-08 10:44:01'),(2,'sidik','41ad70698fd2214e7966a09633933204','2','2013-05-03 09:03:38','2013-05-03 09:03:40'),(3,'4dm1n','eccbc87e4b5ce2fe28308fd9f2a7baf3','1','2013-05-03 09:04:14','2013-05-03 09:04:17');
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-09 17:02:09
